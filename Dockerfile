
# Используем базовый образ Python
FROM python:3.9

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Добавляем файлы из текущей директории в контейнер
ADD . /app

# Устанавливаем зависимости
RUN pip install --no-cache-dir -r requirements_app.txt

# Определяем команду запуска приложения
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]